########## FROZEN STAGE ##########
FROM ros:humble AS frozen_stage

########## CACHE BREAKER STAGE ##########
FROM  frozen_stage AS cache_breaker_stage

RUN mkdir -p crazyflie_ws/src/crazyswarm2

COPY . /crazyflie_ws/src/crazyswarm2
RUN rm -rf /crazyflie_ws/src/crazyswarm2/Docker
COPY Docker/crazyflie-firmware /crazyflie-firmware

########## BASE STAGE ##########
FROM frozen_stage AS base_stage

RUN apt update && apt install -y build-essential cmake \
    libboost-program-options-dev libusb-1.0-0-dev \
    autoconf automake asciidoctor wget pcre2-utils\
    libeigen3-dev asciidoctor iputils-ping \
    git gcc ros-humble-tf-transformations \
    python3 python3-pip python3-pydocstyle nano \
    ros-humble-motion-capture-tracking ros-humble-ament-cmake-python

RUN pip3 install cflib transforms3d rowan nicegui mlagents mlagents-envs
RUN apt install -y libc6-dev

########## DEV BASE STAGE ##########
FROM base_stage AS dev_base_stage

COPY --from=cache_breaker_stage /crazyflie-firmware /crazyflie-firmware

RUN wget https://sourceforge.net/projects/swig/files/swig/swig-4.2.1/swig-4.2.1.tar.gz \
    && tar -xvf swig-4.2.1.tar.gz && cd swig-4.2.1 \
    && ./configure && make && make install

RUN cd crazyflie-firmware && make cf2_defconfig \
    && make bindings_python && cd build \
    && python3 setup.py install --user

ENV PYTHONPATH=$PYTHONPATH:/crazyflie-firmware/build

########## BUILD STAGE ##########
FROM dev_base_stage AS build_stage

COPY Docker/olive_utils /crazyflie_ws/src/olive_utils

RUN . /opt/ros/humble/setup.sh && \ 
    cd /crazyflie_ws && colcon build --symlink-install

########## DEV STAGE ##########
FROM build_stage AS dev_stage

COPY /Docker/entrypoint.sh /entrypoint.sh
WORKDIR /crazyflie_ws
ENTRYPOINT ["/entrypoint.sh"]
CMD tail -f /dev/null

########## PRODUCTION BUILD STAGE ##########
FROM build_stage AS prod_build_stage


########## PRODUCTION STAGE ##########
FROM base_stage AS prod_stage


########## CONTINUOUS INTEGRATION STAGE ##########
FROM dev_stage AS ci_stage


########## LIVE TEST STAGE ##########
FROM dev_stage AS live_test_stage
