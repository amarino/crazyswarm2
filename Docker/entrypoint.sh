#!/bin/bash
set -e

source /opt/ros/humble/setup.bash
source /crazyflie_ws/install/setup.bash

exec "$@"
