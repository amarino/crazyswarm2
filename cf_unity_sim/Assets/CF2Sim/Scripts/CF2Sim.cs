using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using System.Text;
using System;
using System.Collections.Generic;
public class CF2Sim : SideChannel
{
    List<Tuple<Vector3, Quaternion>> poseList = new List<Tuple<Vector3, Quaternion>>();
    bool changed = false;
    public CF2Sim()
    {
        ChannelId = new Guid("621f0a70-4f87-11ea-a6bf-784f4387d1f7");
    }

    public List<Tuple<Vector3, Quaternion>> getPoseList() { return poseList; }

    public bool getChanged() { return changed; }

    public void setChanged(bool value) { changed = value; }


    protected override void OnMessageReceived(IncomingMessage msg)
    {
        var starting_pose = msg.ReadFloatList();
        int j = 0;
        for (; j < starting_pose.Count; j++)
        {

            try
            {
                poseList.Add(new Tuple<Vector3, Quaternion>(new Vector3(starting_pose[j],
                                                    starting_pose[j + 2],
                                                    starting_pose[j + 1]),
                                            new Quaternion(starting_pose[j + 3],
                                            starting_pose[j + 5],
                                            starting_pose[j + 4],
                                            -starting_pose[j + 6])));
            }
            catch (Exception e)
            {
                Debug.LogError("Error while reading the list: " + e.Message);
                return;
            }
            j += 6;

        }

        changed = true;
    }

    public void SendDebugStatementToPython(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            var stringToSend = type.ToString() + ": " + logString + "\n" + stackTrace;
            using (var msgOut = new OutgoingMessage())
            {
                msgOut.WriteString(stringToSend);
                QueueMessageToSend(msgOut);
            }
        }
    }
}
