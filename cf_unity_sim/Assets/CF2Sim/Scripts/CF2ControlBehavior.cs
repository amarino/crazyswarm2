using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class CF2ControlBehavior : Agent
{
    Rigidbody rBody;
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public Transform Target;
    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0, 0);
        }

    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Agent pose
        sensor.AddObservation(this.transform.localPosition.z);
        sensor.AddObservation(-this.transform.localPosition.x);
        sensor.AddObservation(this.transform.localPosition.y);

        var rotation = this.transform.localRotation;

        sensor.AddObservation(-rotation.z);
        sensor.AddObservation(rotation.x);
        sensor.AddObservation(-rotation.y);
        sensor.AddObservation(rotation.w);

        // Agent twist
        sensor.AddObservation(rBody.velocity.z);
        sensor.AddObservation(-rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.y);
        sensor.AddObservation(rBody.angularVelocity.z);
        sensor.AddObservation(-rBody.angularVelocity.x);
        sensor.AddObservation(rBody.angularVelocity.y);

    }

    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 6

        Vector3 controlSignal = Vector3.zero;
        Vector3 torque = Vector3.zero;

        controlSignal.z = actionBuffers.ContinuousActions[0];
        controlSignal.x = -actionBuffers.ContinuousActions[1];
        controlSignal.y = actionBuffers.ContinuousActions[2];
        /*
        torque.z = actionBuffers.ContinuousActions[3];
        torque.x = -actionBuffers.ContinuousActions[4];
        torque.y = actionBuffers.ContinuousActions[5];
        */
        rBody.AddRelativeForce(controlSignal);
        rBody.AddRelativeTorque(torque);

        /*
        Vector3 position = Vector3.zero;
        Quaternion quaternion = new Quaternion();
        Vector3 velocity = Vector3.zero;
        Vector3 angularVelocity = Vector3.zero;

        position.z = actionBuffers.ContinuousActions[0];
        position.x = -actionBuffers.ContinuousActions[1];
        position.y = actionBuffers.ContinuousActions[2];

        velocity.z = actionBuffers.ContinuousActions[7];
        velocity.x = -actionBuffers.ContinuousActions[8];
        velocity.y = actionBuffers.ContinuousActions[9];

        angularVelocity.z = actionBuffers.ContinuousActions[10];
        angularVelocity.x = -actionBuffers.ContinuousActions[11];
        angularVelocity.y = actionBuffers.ContinuousActions[12];

        quaternion.z = actionBuffers.ContinuousActions[3];
        quaternion.x = -actionBuffers.ContinuousActions[4];
        quaternion.y = actionBuffers.ContinuousActions[5];
        quaternion.w = -actionBuffers.ContinuousActions[6];

        this.rBody.angularVelocity = angularVelocity;
        this.rBody.velocity = velocity;
        this.transform.localPosition = position;
        this.transform.localRotation =  quaternion;
        */
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[5] = Input.GetAxis("Horizontal");
        continuousActionsOut[2] = 0.034f * 9.81f + Input.GetAxis("Vertical");
    }
}
