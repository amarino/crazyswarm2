using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents.SideChannels;
using Unity.MLAgents;
using Mujoco;


public class SettingsSim : MonoBehaviour
{

    private CF2Sim settingsChannel;
    public GameObject dronePrefab;

    public Transform parent;

    private List<Tuple<Vector3, Quaternion>> poseList;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void Awake()
    {
        // We create the Side Channel
        settingsChannel = new CF2Sim();

        // The channel must be registered with the SideChannelManager class
        SideChannelManager.RegisterSideChannel(settingsChannel);
    }

    public void OnDestroy()
    {
        // De-register the Debug.Log callback
        if (Academy.IsInitialized)
        {
            SideChannelManager.UnregisterSideChannel(settingsChannel);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (settingsChannel.getChanged())
        {
            settingsChannel.setChanged(false);
            poseList = settingsChannel.getPoseList();
            int j = 0;
            foreach (var pose in poseList)
            {
                Debug.Log("Creating drone pose: " + pose.Item1 + " " + pose.Item2);
                var drone = Instantiate(dronePrefab, pose.Item1, pose.Item2, parent);
                drone.name = "CF2_" + j.ToString();
                //drone.transform.FindChild("cf2 Body").GetComponent<CF2ControlBehavior_MJ>().id = "";
                j = j + 1;
            }

            //var scene = MjScene.Instance;
            //scene.CreateScene(true);
        }

    }
}
