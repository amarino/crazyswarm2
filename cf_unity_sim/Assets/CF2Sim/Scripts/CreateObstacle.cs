using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Unity.MLAgents.SideChannels;
using Unity.MLAgents;

public class CreateObstacle : MonoBehaviour
{
    public GameObject obstaclePrefab;

    public List<GameObject> obstacleList;

    private ObstacleChannel obstacleChannel;

    public Transform parent;

    Vector3 cube = new Vector3(0.2f, 0.2f, 0.2f);
    Vector3 pilar = new Vector3(0.2f, 2.0f, 0.2f);
    // Start is called before the first frame update
    void Start()
    {
        obstacleList = new List<GameObject>();
        obstacleChannel = new ObstacleChannel();

        SideChannelManager.RegisterSideChannel(obstacleChannel);

    }

    public void OnDestroy()
    {
        // De-register the Debug.Log callback
        if (Academy.IsInitialized)
        {
            SideChannelManager.UnregisterSideChannel(obstacleChannel);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (obstacleChannel.getChanged())
        {
            obstacleChannel.setChanged(false);
            var poseScaleList = obstacleChannel.getPoseList();
            Debug.Log(poseScaleList.Count.ToString());

            for (int j = 0; j < poseScaleList.Count; j++)
            {
                if (obstacleList.Count <= j)
                {
                    Debug.Log("Creating obstacle pose: " + poseScaleList[j].Item1 + " " + poseScaleList[j].Item2);
                    var obstacle = Instantiate(obstaclePrefab, poseScaleList[j].Item1, new Quaternion(), parent);
                    obstacle.transform.localScale = poseScaleList[j].Item2;
                    obstacle.name = "Obstacle_" + j.ToString();
                    obstacleList.Add(obstacle);
                }
                else
                {
                    obstacleList[j].transform.position = poseScaleList[j].Item1;
                    obstacleList[j].transform.localScale = poseScaleList[j].Item2;
                }

            }
        }
    }
}
