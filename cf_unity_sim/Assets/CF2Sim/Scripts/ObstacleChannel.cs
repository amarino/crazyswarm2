using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using System.Text;
using System;
using System.Collections.Generic;
public class ObstacleChannel : SideChannel
{
    List<Tuple<Vector3, Vector3>> poseScaleList;
    bool changed = false;
    public ObstacleChannel()
    {
        ChannelId = new Guid("621f0a70-4f87-11ea-a6bf-784f4387d1f1");
        poseScaleList = new List<Tuple<Vector3, Vector3>>();
    }

    public List<Tuple<Vector3, Vector3>> getPoseList() { return poseScaleList; }

    public bool getChanged() { return changed; }

    public void setChanged(bool value) { changed = value; }


    protected override void OnMessageReceived(IncomingMessage msg)
    {
        var starting_pose = msg.ReadFloatList();
        Debug.Log("received");
        int j = 0;
        poseScaleList = new List<Tuple<Vector3, Vector3>>();
        for (; j < starting_pose.Count; j++)
        {

            try
            {
                poseScaleList.Add(new Tuple<Vector3, Vector3>(new Vector3(starting_pose[j],
                                                    starting_pose[j + 2],
                                                    starting_pose[j + 1]),
                                            new Vector3(starting_pose[j + 3],
                                            starting_pose[j + 5],
                                            starting_pose[j + 4])));
            }
            catch (Exception e)
            {
                Debug.LogError("Error while reading the list: " + e.Message);
                return;
            }
            j += 5;

        }

        changed = true;
    }

}
