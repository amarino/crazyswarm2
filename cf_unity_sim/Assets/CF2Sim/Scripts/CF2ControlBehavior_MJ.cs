using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.SideChannels;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using Mujoco;
using System;
using System.Collections.Generic;


public class CF2ControlBehavior_MJ : Agent
{
    // two digits id
    public Mujoco.MjBody mjBody;
    public Mujoco.MjFreeJoint joint;
    Mujoco.MjScene scene;

    float arm = 0.707106781f * 0.046f;// 0.707106781 * arm_length;
    float t2t = 0.006f;
    private float[] p = new float[3] { 2.55077341e-08f, -4.92422570e-05f, -1.51910248e-01f };

    Vector3 start_position;


    void Start()
    {
        scene = MjScene.Instance;
        start_position = this.transform.localPosition;
    }

    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0)
        {
            //this.mjBody.angularVelocity = Vector3.zero;
            //this.mjBody.velocity = Vector3.zero;
            this.transform.localPosition = start_position;
        }

        this.RequestDecision();

    }

    public override unsafe void CollectObservations(VectorSensor sensor)
    {

        scene = MjScene.Instance;
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 1]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 2]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 4]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 5]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 6]);
        sensor.AddObservation((float)scene.Data->qpos[joint.QposAddress + 3]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress + 1]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress + 2]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress + 3]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress + 4]);
        sensor.AddObservation((float)scene.Data->qvel[joint.DofAddress + 5]);

        // Agent pose
        //sensor.AddObservation(this.transform.localPosition.z);
        //sensor.AddObservation(-this.transform.localPosition.x);
        //sensor.AddObservation(this.transform.localPosition.y);

        //var rotation = this.transform.localRotation;
        //sensor.AddObservation(-rotation.z);
        //sensor.AddObservation(rotation.x);
        //sensor.AddObservation(-rotation.y);
        //sensor.AddObservation(rotation.w);

        // Agent twist

        //sensor.AddObservation(mjBody.velocity.z);
        //sensor.AddObservation(-mjBody.velocity.x);
        //sensor.AddObservation(mjBody.velocity.y);
        //sensor.AddObservation(mjBody.angularVelocity.z);
        //sensor.AddObservation(-mjBody.angularVelocity.x);
        //sensor.AddObservation(mjBody.angularVelocity.y);

    }

    public override unsafe void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 6
        (Vector3 force, Vector3 torque) = compute_ft(actionBuffers);

        Quaternion quaternion = new Quaternion((float)scene.Data->qpos[joint.QposAddress + 4],
                                            (float)scene.Data->qpos[joint.QposAddress + 5],
                                            (float)scene.Data->qpos[joint.QposAddress + 6],
                                            (float)scene.Data->qpos[joint.QposAddress + 3]);

        force = quaternion * force;

        scene = MjScene.Instance;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 0] = force.x;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 1] = force.y;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 2] = force.z;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 3] = torque.x;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 4] = torque.y;
        scene.Data->qfrc_applied[6 * joint.MujocoId + 5] = torque.z;

        /*
        Vector3 position = Vector3.zero;
        Quaternion quaternion = new Quaternion();
        Vector3 velocity = Vector3.zero;
        Vector3 angularVelocity = Vector3.zero;

        position.z = actionBuffers.ContinuousActions[0];
        position.x = -actionBuffers.ContinuousActions[1];
        position.y = actionBuffers.ContinuousActions[2];

        velocity.z = actionBuffers.ContinuousActions[7];
        velocity.x = -actionBuffers.ContinuousActions[8];
        velocity.y = actionBuffers.ContinuousActions[9];

        angularVelocity.z = actionBuffers.ContinuousActions[10];
        angularVelocity.x = -actionBuffers.ContinuousActions[11];
        angularVelocity.y = actionBuffers.ContinuousActions[12];

        quaternion.z = actionBuffers.ContinuousActions[3];
        quaternion.x = -actionBuffers.ContinuousActions[4];
        quaternion.y = actionBuffers.ContinuousActions[5];
        quaternion.w = -actionBuffers.ContinuousActions[6];

        this.rBody.angularVelocity = angularVelocity;
        this.rBody.velocity = velocity;
        this.transform.localPosition = position;
        this.transform.localRotation =  quaternion;
        */
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[5] = Input.GetAxis("Horizontal");
        continuousActionsOut[2] = 0.034f * 9.81f + Input.GetAxis("Vertical");
    }

    private float[] rpm_to_force(float[] rpm)
    {
        var force_in_newton = new float[4]{
            9.81f*(p[0]*Mathf.Pow(rpm[0],2) + p[1]*rpm[0] + p[2])/1000.0f,
            9.81f*(p[0]*Mathf.Pow(rpm[1],2) + p[1]*rpm[1] + p[2])/1000.0f,
            9.81f*(p[0]*Mathf.Pow(rpm[2],2) + p[1]*rpm[2] + p[2])/1000.0f,
            9.81f*(p[0]*Mathf.Pow(rpm[3],2) + p[1]*rpm[3] + p[2])/1000.0f
        };

        return force_in_newton;
    }

    private Tuple<Vector3, Vector3> compute_ft(ActionBuffers rpm)
    {
        /*
        var force_in_newton = rpm_to_force(new float[4] { rpm.ContinuousActions[0],
                                                          rpm.ContinuousActions[1],
                                                          rpm.ContinuousActions[2],
                                                          rpm.ContinuousActions[3] });
        */
        var force_in_newton = new float[4]{rpm.ContinuousActions[0],
                                           rpm.ContinuousActions[1],
                                           rpm.ContinuousActions[2],
                                           rpm.ContinuousActions[3]};

        Debug.Log(force_in_newton[0].ToString() + " " + force_in_newton[1].ToString() + " " + force_in_newton[2].ToString() + " " + force_in_newton[3].ToString());

        var eta = new float[4]{
            force_in_newton[0]+force_in_newton[1]+force_in_newton[2]+force_in_newton[3],
            -arm*force_in_newton[0]-arm*force_in_newton[1]+arm*force_in_newton[2]+arm*force_in_newton[3],
            -arm*force_in_newton[0]+arm*force_in_newton[1]+arm*force_in_newton[2]-arm*force_in_newton[3],
            -t2t*force_in_newton[0]+t2t*force_in_newton[1]-t2t*force_in_newton[2]+t2t*force_in_newton[3]
        };

        Vector3 force = new Vector3(0.0f, 0.0f, eta[0]);
        Vector3 torque = new Vector3(eta[1], eta[2], eta[3]);
        return new Tuple<Vector3, Vector3>(force, torque);
    }
}
