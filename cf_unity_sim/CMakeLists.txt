cmake_minimum_required(VERSION 3.8)
project(cf_unity_sim)

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_python REQUIRED)

# Install Python modules
ament_python_install_package(cf_unity_sim)

# # Install launch and config files.
# install(DIRECTORY
#   config
#   data
#   launch
#   DESTINATION share/${PROJECT_NAME}/
# )

ament_package()
