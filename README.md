[![ROS 2](https://github.com/IMRCLab/crazyswarm2/actions/workflows/ci-ros2.yml/badge.svg)](https://github.com/IMRCLab/crazyswarm2/actions/workflows/ci-ros2.yml)

# Crazyswarm2 + Mujoco&Unity simulation
<img src="example_mu1.png"  width="300"/>  <img src="example_mu2.png"  width="300"/>

Clone the repository 

``` shell
git clone --recurse-submodules --shallow-submodules https://gitlab.inria.fr/amarino/crazyswarm2_unity_sim.git
```

Install instructions for crazyswarm2 available [here](https://imrclab.github.io/crazyswarm2/).

Install [mlagents](https://github.com/Unity-Technologies/ml-agents/tree/develop) for communication with Unity

```shell
pip3 install mlagents
```

## Docker build

ensure to have clone the repository together with its submodules

place yourself in the crazyswarm2_unity_sim and build the image crazyswarm2-mju

```shell
docker build -f /Docker/Dockerfile --target dev_stage -t crazyswarm2-mju .
```

