from mlagents_envs.environment import UnityEnvironment
from mlagents_envs.environment import ActionTuple
from mlagents_envs.side_channel.side_channel import (
    SideChannel,
    IncomingMessage,
    OutgoingMessage,
)
from mlagents_envs.side_channel.engine_configuration_channel import (
    EngineConfigurationChannel,
)
import uuid


import numpy as np
from rclpy.node import Node
from rosgraph_msgs.msg import Clock
import time
from crazyflie_sim.sim_data_types import Action, State
import cf_unity_sim


class Backend:
    """Backend that uses Unity+mujoco to simulate the rigid-body dynamics."""

    def __init__(
        self,
        node: Node,
        names: list[str],
        states: list[State],
        scene: str = None,
        yaw: list[float] = None,
    ):
        self.node = node
        self.names = names
        if self.node != None:
            self.clock_publisher = node.create_publisher(Clock, "clock", 10)
        self.t = 0
        self.dt = 0.0005

        # Create the channel
        self.setting_channel = SettingsChannel()
        self.engine_configuration_channel = EngineConfigurationChannel()
        self.obstacle_channel = ObstaclesChannel()
        # Open the environment from Unity executable

        if scene == None or scene not in cf_unity_sim.available_build.keys():
            print("the scene must be one of the following:")
            for key in cf_unity_sim.available_build.keys():
                print(f"{key} : {cf_unity_sim.available_build[key]}")
            crazyflie_sim_path = None
        else:
            crazyflie_sim_path = cf_unity_sim.__file__.split("__init__.py")[0] + scene + ".x86_64"

        # We start the communication with the Unity Editor and pass the string_log side channel as input
        self.env = UnityEnvironment(
            file_name=crazyflie_sim_path,
            side_channels=[
                self.setting_channel,
                self.engine_configuration_channel,
                self.obstacle_channel,
            ],
            no_graphics=False,
        )

        self.engine_configuration_channel.set_configuration_parameters(
            target_frame_rate=30, quality_level=0, time_scale=self.dt / 0.0005
        )

        # Send the initial state
        data = []
        for state in states:
            data.append(state.pos[0])
            data.append(state.pos[1])
            data.append(state.pos[2])
            data.append(state.quat[1])
            data.append(state.quat[2])
            data.append(state.quat[3])
            data.append(state.quat[0])

        self.setting_channel.send_float_list(data)

        self.env.reset()

        behavior_name = list(self.env.behavior_specs)[0]
        print(f"Name of the behavior : {behavior_name}")

        # Get the behavior spec of the environment
        self.group_name = list(self.env.behavior_specs.keys())[0]  # Get the first group_name
        self.group_spec = self.env.behavior_specs[self.group_name]

        i = 0
        self.uavs = {}
        for name, state in zip(names, states):
            self.uavs[name] = i
            i += 1

    def time(self) -> float:
        return self.t

    def step(self, states_desired: list[State], actions: list[Action], name: str) -> list[State]:
        """
        for name, action in zip(self.names, states_desired):
            id = self.uavs[name]
            action_g = np.array([[action.pos[0],
                                 action.pos[1],
                                 action.pos[2],
                      action.quat[0],
                      action.quat[1],
                      action.quat[2],
                      action.quat[3],
                      action.vel[0],
                      action.vel[1],
                      action.vel[2],
                      action.omega[0],
                      action.omega[1],
                      action.omega[2]]])
            print(action_g)
            action_data = ActionTuple(action_g)
            self.env.set_action_for_agent(self.group_name,id,action_data)
        """
        # time_start = time.time()
        action_data = ActionTuple(np.array([action.rpm for action in actions]))
        self.env.set_actions(self.group_name, action_data)
        self.env.step()  # Move the simulation forward
        decision_steps, _ = self.env.get_steps(self.group_name)

        next_states = unity2sim_state(decision_steps.obs)
        # print(f"Time to step : {time.time()-time_start}")
        return next_states

    def shutdown(self):
        self.env.close()

    def send_obstacle(self, obstacles):
        self.obstacle_channel.send_float_list(obstacles)


# Create the SettingsChannel class
class SettingsChannel(SideChannel):

    def __init__(self) -> None:
        super().__init__(uuid.UUID("621f0a70-4f87-11ea-a6bf-784f4387d1f7"))

    def on_message_received(self, msg: IncomingMessage) -> None:
        """
        Note: We must implement this method of the SideChannel interface to
        receive messages from Unity
        """
        # We simply read a string from the message and print it.
        print(msg.read_string())

    def send_float_list(self, data: list) -> None:
        # Add the string to an OutgoingMessage
        msg = OutgoingMessage()
        msg.write_float32_list(data)
        # We call this method to queue the data we want to send
        super().queue_message_to_send(msg)


# Create the SettingsChannel class
class ObstaclesChannel(SideChannel):

    def __init__(self) -> None:
        super().__init__(uuid.UUID("621f0a70-4f87-11ea-a6bf-784f4387d1f1"))

    def on_message_received(self, msg: IncomingMessage) -> None:
        """
        Note: We must implement this method of the SideChannel interface to
        receive messages from Unity
        """
        pass

    def send_float_list(self, data: list) -> None:
        # Add the string to an OutgoingMessage
        msg = OutgoingMessage()
        msg.write_float32_list(data)
        # We call this method to queue the data we want to send
        super().queue_message_to_send(msg)


def unity2sim_state(q):
    results = []
    for qi in q[0]:
        result = State()
        result.pos = qi[0:3]
        result.quat = np.concatenate((qi[6:7], qi[3:6]))
        result.quat = result.quat / (np.linalg.norm(result.quat) + 1e-9)
        result.vel = qi[7:10]
        result.omega = qi[10:13]
        results.append(result)
    return results


# test
if __name__ == "__main__":
    backend = Backend(
        None,
        [
            "drone1",
            "drone2",
            "drone3",
            "drone4",
            "drone5",
            "drone6",
            "drone7",
            "drone8",
        ],
        [State(pos=3 * np.random.rand(3)) for _ in range(8)],
        scene="ObstacleSim",
    )
    for i in range(10000):
        backend.step(State(), [Action([0, 0, 0, 0]) for _ in range(8)], None)
    print("done")
