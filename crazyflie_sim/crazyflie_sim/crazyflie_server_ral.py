#!/usr/bin/env python3

"""
A crazyflie server for simulation for mader.

    2024 - Antonio Marino
"""

from functools import partial
import importlib
from rclpy.executors import MultiThreadedExecutor
from crazyflie_interfaces.msg import FullState, Hover
from crazyflie_interfaces.srv import GoTo
from olive_interfaces.srv import Takeoff, Land

from snapstack_msgs_ros2.msg import Goal
from snapstack_msgs_ros2.msg import State as StateMader
from crazyflie_interfaces.srv import (
    NotifySetpointsStop,
    StartTrajectory,
    UploadTrajectory,
)
import numpy as np
from threading import Thread
from geometry_msgs.msg import Twist, Pose
import rclpy
from rclpy.node import Node
from scipy.spatial.transform import Rotation
from std_srvs.srv import _empty, Empty
from visualization_msgs.msg import Marker, MarkerArray
import time
from time import sleep
from rclpy.time import Time
from rosgraph_msgs.msg import Clock
from geometry_msgs.msg import TransformStamped
from tf2_ros import TransformBroadcaster

# import BackendRviz from .backend_rviz
# from .backend import *
# from .backend.none import BackendNone
from .crazyflie_sil import CrazyflieSIL, TrajectoryPolynomialPiece
from .sim_data_types import State, Action


class CrazyflieServer(Node):

    def __init__(self):
        super().__init__(
            "crazyflie_server",
            allow_undeclared_parameters=True,
            automatically_declare_parameters_from_overrides=True,
        )

        # Turn ROS parameters into a dictionary
        self._ros_parameters = self._param_to_dict(self._parameters)
        self.cfs = {}

        self.world_tf_name = "world"
        try:
            self.world_tf_name = self._ros_parameters["world_tf_name"]
        except KeyError:
            pass
        robot_data = self._ros_parameters["robots"]

        # Parse robots
        names = []
        initial_states = []
        self.initial_yaw = []
        for cfname in robot_data:
            if robot_data[cfname]["enabled"]:
                type_cf = robot_data[cfname]["type"]
                # do not include virtual objects
                connection = self._ros_parameters["robot_types"][type_cf].get(
                    "connection", "crazyflie"
                )
                if connection == "crazyflie":
                    names.append(cfname)
                    pos = robot_data[cfname]["initial_position"][:3]
                    yaw = robot_data[cfname]["initial_position"][-1]
                    initial_states.append(State(pos))
                    self.initial_yaw.append(np.radians(yaw))

        # initialize backend by dynamically loading the module
        backend_name = self._ros_parameters["sim"]["backend"]
        module = importlib.import_module(".backend." + backend_name, package="crazyflie_sim")
        class_ = getattr(module, "Backend")
        if backend_name == "unity":
            scene = self._ros_parameters["sim"]["scene"]
            self.backend = class_(self, names, initial_states, scene)
        else:
            self.backend = class_(self, names, initial_states)

        # initialize visualizations by dynamically loading the modules
        self.visualizations = []
        for vis_key in self._ros_parameters["sim"]["visualizations"]:
            if self._ros_parameters["sim"]["visualizations"][vis_key]["enabled"]:
                module = importlib.import_module(
                    ".visualization." + str(vis_key), package="crazyflie_sim"
                )
                class_ = getattr(module, "Visualization")
                vis = class_(
                    self,
                    self._ros_parameters["sim"]["visualizations"][vis_key],
                    names,
                    initial_states,
                )
                self.visualizations.append(vis)

        controller_name = backend_name = self._ros_parameters["sim"]["controller"]

        # create robot SIL objects
        for name, initial_state in zip(names, initial_states):
            self.cfs[name] = CrazyflieSIL(
                name, initial_state.pos, controller_name, self.backend.time
            )

        # Mader interface
        self.clock_publisher = self.create_publisher(Clock, "clock", 10)
        self.tf_broadcaster = TransformBroadcaster(self)
        self.obstacle_horiz_sub = self.create_subscription(
            Marker, "/shapes_static_horiz", self.obst_horiz, 1
        )
        self.obstacle_vert_sub = self.create_subscription(
            Marker, "/shapes_static_vert", self.obst_vert, 1
        )
        self.obstacle_dynamic_sub = self.create_subscription(
            Marker, "/shapes_dynamic", self.obst_dynamic, 1
        )
        self.obstacle_horiz = []
        self.obstacle_vert = []
        self.obstacle_dynamic = []

        # Create services for the entire swarm and each individual crazyflie
        self.create_service(Empty, "all/emergency", self._emergency_callback)
        self.create_service(Takeoff, "all/takeoff", self._takeoff_callback)
        self.create_service(Land, "all/land", self._land_callback)
        self.create_service(GoTo, "all/go_to", self._go_to_callback)
        self.create_service(
            StartTrajectory,
            "all/start_trajectory",
            self._start_trajectory_callback,
        )

        self.drone_pub: dict = {}
        for name, _ in self.cfs.items():
            self.create_service(
                Empty,
                name + "/emergency",
                partial(self._emergency_callback, name=name),
            )
            self.create_service(
                Takeoff,
                name + "/takeoff",
                partial(self._takeoff_callback, name=name),
            )
            self.create_service(Land, name + "/land", partial(self._land_callback, name=name))
            self.create_service(GoTo, name + "/go_to", partial(self._go_to_callback, name=name))
            self.create_service(
                StartTrajectory,
                name + "/start_trajectory",
                partial(self._start_trajectory_callback, name=name),
            )
            self.create_service(
                UploadTrajectory,
                name + "/upload_trajectory",
                partial(self._upload_trajectory_callback, name=name),
            )
            self.create_service(
                NotifySetpointsStop,
                name + "/notify_setpoints_stop",
                partial(self._notify_setpoints_stop_callback, name=name),
            )
            self.create_subscription(
                Twist,
                name + "/cmd_vel_legacy",
                partial(self._cmd_vel_legacy_changed, name=name),
                1,
            )
            self.create_subscription(
                Hover,
                name + "/cmd_hover",
                partial(self._cmd_hover_changed, name=name),
                1,
            )
            self.create_subscription(
                Twist,
                name + "/twist_cmd",
                partial(self._cmd_vel_world, name=name),
                1,
            )
            self.create_subscription(
                FullState,
                name + "/cmd_full_state",
                partial(self._cmd_full_state_changed, name=name),
                1,
            )
            self.create_subscription(
                Goal,
                name + "/goal",
                partial(self._cmd_full_state_mader, name=name),
                1,
            )
            self.drone_pub[name] = self.create_publisher(StateMader, name + "/state", 10)

        # step as fast as possible
        self.max_dt = (
            0.0
            if "max_dt" not in self._ros_parameters["sim"]
            else self._ros_parameters["sim"]["max_dt"]
        )
        print(self.max_dt)
        # self.timer = self.create_timer(0.0, self._timer_callback)
        self.timer_t = self.create_timer(0.01, self._timer_t)

        self.is_shutdown = False

    def on_shutdown_callback(self):
        if not self.is_shutdown:
            self.backend.shutdown()
            for visualization in self.visualizations:
                visualization.shutdown()

            self.is_shutdown = True

    def _once_spin(self):
        while not self.is_shutdown:
            rclpy.spin_once(self)
            time.sleep(self.max_dt)

    def _timer_t(self):
        obstacle = self.obstacle_horiz + self.obstacle_vert + self.obstacle_dynamic
        self.backend.send_obstacle(obstacle)

        for name, _ in self.cfs.items():
            t = TransformStamped()
            state_msg = StateMader()
            state = self.cfs[name].state
            gyro = self.cfs[name].sensors.gyro
            setpoint = self.cfs[name].setpoint
            # Read message content and assign it to
            # corresponding tf variables
            t.header.stamp = self.get_clock().now().to_msg()
            t.header.frame_id = "world"
            t.child_frame_id = name

            t.transform.translation.x = state.position.x
            t.transform.translation.y = state.position.y
            t.transform.translation.z = state.position.z

            t.transform.rotation.x = state.attitudeQuaternion.x
            t.transform.rotation.y = state.attitudeQuaternion.y
            t.transform.rotation.z = state.attitudeQuaternion.z
            t.transform.rotation.w = state.attitudeQuaternion.w

            # Send the transformation
            self.tf_broadcaster.sendTransform(t)
            state_msg.posx = state.position.x
            state_msg.posy = state.position.y
            state_msg.posz = state.position.z
            state_msg.velx = state.velocity.x
            state_msg.vely = state.velocity.y
            state_msg.velz = state.velocity.z
            state_msg.quatx = state.attitudeQuaternion.x
            state_msg.quaty = state.attitudeQuaternion.y
            state_msg.quatz = state.attitudeQuaternion.z
            state_msg.quatw = state.attitudeQuaternion.w
            state_msg.wx = gyro.x
            state_msg.wy = gyro.y
            state_msg.wz = gyro.z
            state_msg.abiasx = setpoint.acceleration.x
            state_msg.abiasy = setpoint.acceleration.y
            state_msg.abiasz = setpoint.acceleration.z

            self.drone_pub[name].publish(state_msg)

    def _timer_callback(self):
        while True:
            # start_time = time.time()
            self.backend.t += self.backend.dt
            actions = []
            states_desired = []

            for name, _ in self.cfs.items():

                # update setpoint
                states_desired.append(self.cfs[name].getSetpoint(self.backend.dt))

                # execute the control loop
                actions.append(self.cfs[name].executeController())

                # execute the physics simulator

            # state = states_desired
            state = self.backend.step(states_desired, actions, None)

            for i, (name, _) in enumerate(self.cfs.items()):
                # update the resulting state
                self.cfs[name].setState(state[i])
                # pose_msg = Pose()
                # pose_msg.position.x = state.pos[0]
                # pose_msg.position.y = state.pos[1]
                # pose_msg.position.z = state.pos[2]
                # pose_msg.orientation.x = state.quat[1]
                # pose_msg.orientation.y = state.quat[2]
                # pose_msg.orientation.z = state.quat[3]
                # pose_msg.orientation.w = state.quat[0]

                # self.drone_pub[name].publish(pose_msg)

                # for vis in self.visualizations:
                #    vis.step(self.backend.time(), states_next, states_desired, actions)

            # print("Time taken for one step: ", time.time() - start_time)

            clock_message = Clock()
            clock_message.clock = Time(seconds=self.backend.time()).to_msg()
            self.clock_publisher.publish(clock_message)
            time.sleep(self.max_dt)

    def _param_to_dict(self, param_ros):
        """Turn ROS 2 parameters from the node into a dict."""
        tree = {}
        for item in param_ros:
            t = tree
            for part in item.split("."):
                if part == item.split(".")[-1]:
                    t = t.setdefault(part, param_ros[item].value)
                else:
                    t = t.setdefault(part, {})
        return tree

    def _emergency_callback(self, request, response, name="all"):
        self.get_logger().info("emergency not yet implemented")

        return response

    def _takeoff_callback(self, request, response, name="all"):
        """Service callback to takeoff the crazyflie."""
        duration = float(request.duration.sec) + float(request.duration.nanosec / 1e9)
        self.get_logger().info(
            f"takeoff(height={request.height} m,"
            + f"duration={duration} s,"
            + f"group_mask={request.group_mask}) {name}"
        )
        cfs = self.cfs if name == "all" else {name: self.cfs[name]}
        for _, cf in cfs.items():
            cf.takeoff(request.height, duration, request.group_mask)

        return response

    def _land_callback(self, request, response, name="all"):
        """Service callback to land the crazyflie."""
        duration = float(request.duration.sec) + float(request.duration.nanosec / 1e9)
        self.get_logger().info(
            f"land(height={request.height} m,"
            + f"duration={duration} s,"
            + f"group_mask={request.group_mask}) {name}"
        )
        cfs = self.cfs if name == "all" else {name: self.cfs[name]}
        for _, cf in cfs.items():
            cf.land(request.height, duration, request.group_mask)

        return response

    def _go_to_callback(self, request, response, name="all"):
        """Service callback to have the crazyflie go to a position."""
        duration = float(request.duration.sec) + float(request.duration.nanosec / 1e9)

        self.get_logger().info(
            "go_to(position=%f,%f,%f m, yaw=%f rad, duration=%f s, relative=%d, group_mask=%d)"
            % (
                request.goal.x,
                request.goal.y,
                request.goal.z,
                request.yaw,
                duration,
                request.relative,
                request.group_mask,
            )
        )
        cfs = self.cfs if name == "all" else {name: self.cfs[name]}
        for _, cf in cfs.items():
            cf.goTo(
                [request.goal.x, request.goal.y, request.goal.z],
                request.yaw,
                duration,
                request.relative,
                request.group_mask,
            )

        return response

    def _notify_setpoints_stop_callback(self, request, response, name="all"):
        self.get_logger().info("Notify setpoint stop not yet implemented")
        return response

    def _upload_trajectory_callback(self, request, response, name="all"):
        self.get_logger().info("Upload trajectory(id=%d)" % (request.trajectory_id))

        cfs = self.cfs if name == "all" else {name: self.cfs[name]}
        for _, cf in cfs.items():
            pieces = []
            for piece in request.pieces:
                poly_x = piece.poly_x
                poly_y = piece.poly_y
                poly_z = piece.poly_z
                poly_yaw = piece.poly_yaw
                duration = float(piece.duration.sec) + float(piece.duration.nanosec / 1e9)
                pieces.append(TrajectoryPolynomialPiece(poly_x, poly_y, poly_z, poly_yaw, duration))
            cf.uploadTrajectory(request.trajectory_id, request.piece_offset, pieces)

        return response

    def _start_trajectory_callback(self, request, response, name="all"):
        self.get_logger().info(
            "start_trajectory(id=%d, timescale=%f, reverse=%d, relative=%d, group_mask=%d)"
            % (
                request.trajectory_id,
                request.timescale,
                request.reversed,
                request.relative,
                request.group_mask,
            )
        )
        cfs = self.cfs if name == "all" else {name: self.cfs[name]}
        for _, cf in cfs.items():
            cf.startTrajectory(
                request.trajectory_id,
                request.timescale,
                request.reversed,
                request.relative,
                request.group_mask,
            )

        return response

    def _cmd_vel_legacy_changed(self, msg, name=""):
        """
        Topic update callback.

        Controls the attitude and thrust of the crazyflie with teleop.
        """
        self.get_logger().info("cmd_vel_legacy not yet implemented")

    def _cmd_vel_world(self, msg, name=""):
        """
        Topic update callback.

        Controls the world velocity.
        """
        vel = [msg.linear.x, msg.linear.y, msg.linear.z]
        yawrate = msg.angular.z
        self.cfs[name].cmdWorldVel(vel, yawrate)

    def _cmd_hover_changed(self, msg, name=""):
        """
        Topic update callback for hover command.

        Used from the velocity multiplexer (vel_mux).
        """
        self.get_logger().info("cmd_hover not yet implemented")

    def _cmd_full_state_changed(self, msg, name):
        q = [
            msg.pose.orientation.w,
            msg.pose.orientation.x,
            msg.pose.orientation.y,
            msg.pose.orientation.z,
        ]
        rpy = Rotation.from_quat(q).as_euler("xyz")

        self.cfs[name].cmdFullState(
            [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z],
            [msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z],
            [msg.acc.x, msg.acc.y, msg.acc.z],
            rpy[2],
            [msg.twist.angular.x, msg.twist.angular.y, msg.twist.angular.z],
        )

    def _cmd_full_state_mader(self, msg: Goal, name: str):

        psi = self.cfs[name].cmdHl_yaw
        dpsi = msg.dpsi

        self.cfs[name].cmdFullState(
            [msg.px, msg.py, msg.pz],
            [msg.vx, msg.vy, msg.vz],
            [msg.ax, msg.ay, msg.az],
            psi,
            [0.0, 0.0, dpsi],
        )

    def obst_horiz(self, msg):
        self.obstacle_horiz = []
        for marker in msg.points:
            self.obstacle_horiz.extend(
                (
                    marker.x,
                    marker.y,
                    marker.z,
                    msg.scale.x,
                    msg.scale.y,
                    msg.scale.z,
                )
            )

    def obst_vert(self, msg):
        self.obstacle_vert = []
        for marker in msg.points:
            self.obstacle_vert.extend(
                (
                    marker.x,
                    marker.y,
                    marker.z,
                    msg.scale.x,
                    msg.scale.y,
                    msg.scale.z,
                )
            )

    def obst_dynamic(self, msg):
        self.obstacle_dynamic = []
        for marker in msg.points:
            self.obstacle_dynamic.extend(
                (
                    marker.x,
                    marker.y,
                    marker.z,
                    msg.scale.x,
                    msg.scale.y,
                    msg.scale.z,
                )
            )


def main(args=None):

    rclpy.init(args=args)
    crazyflie_server = CrazyflieServer()
    rclpy.get_default_context().on_shutdown(crazyflie_server.on_shutdown_callback)

    spin_thread = Thread(target=crazyflie_server._once_spin)
    spin_thread.start()

    for i, (_, cf) in enumerate(crazyflie_server.cfs.items()):
        cf.takeoff(cf.state.position.z + 1.0, 2.5, 0, crazyflie_server.initial_yaw[i])

    try:
        crazyflie_server._timer_callback()
    except KeyboardInterrupt:
        crazyflie_server.on_shutdown_callback()
    finally:
        spin_thread.join()
        rclpy.try_shutdown()
        crazyflie_server.destroy_node()


if __name__ == "__main__":
    main()
